"use strict";

// ## Теоретичні питання

// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.

// setTimeout() - затримка запуску функціі один раз,
// setInterval() - регулярне повторення функціі через встановленний інтервал.

// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?

// Це планує виклик функціі настільки швидко, наскільки це можливо.
// Але планувальник викликатиме функцію лише після завершення виконання поточного коду.

// 3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?

// Тому, що setInterval() самостійно не зупиниться.

// #### Технічні вимоги:

// - У папці `banners` лежить HTML код та папка з картинками.
// - При запуску програми на екрані має відображатись перша картинка.
// - Через 3 секунди замість неї має бути показано друга картинка.
// - Ще через 3 секунди – третя.
// - Ще через 3 секунди – четверта.
// - Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// - Після запуску програми десь на екрані має з'явитись кнопка з написом `Припинити`.
// - Після натискання на кнопку `Припинити` цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// - Поруч із кнопкою `Припинити` має бути кнопка `Відновити показ`, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

const images = document.querySelectorAll("img");
let i = 0;

const divImages = document.querySelector(".images-wrapper");

const btnStop = document.createElement("button");
btnStop.type = "button";
btnStop.innerText = "Припинити";

const btnStart = document.createElement("button");
btnStart.type = "button";
btnStart.innerText = "Відновити показ";

function nextImg() {
  if (i === images.length - 1) {
    images[i].style.display = "none";
    i = 0;
    images[0].style.display = "block";
  } else {
    images[i].style.display = "none";
    images[i + 1].style.display = "block";
    i++;
  }
  divImages.after(btnStart);
  divImages.after(btnStop);
}

let cycleImage = setInterval(nextImg, 3000);

btnStop.addEventListener("click", () => {
  clearInterval(cycleImage);
  btnStop.setAttribute("disabled", true);
  btnStart.removeAttribute("disabled");
});
btnStart.addEventListener("click", () => {
  cycleImage = setInterval(nextImg, 3000);
  btnStart.setAttribute("disabled", true);
  btnStop.removeAttribute("disabled");
});
 